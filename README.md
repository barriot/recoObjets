# Projet long

### Intégration d'une fonction de reconnaissance d'objets

L’objectif technique du projet est d’intégrer une fonction de reconnaissance d’objets dans un petit robot mobile.  
Ce travail s’intègre dans le cadre d’un projet de recherche en cours focalisé sur la mise en œuvre d’architectures multi-cœur et FPGA pour les systèmes embarqués et de travaux à venir concernant la mise en œuvre, la vérification et validation d’algorithmes de type « machine learning ».

### Les travaux comporteront les activités suivantes :

* Analyse des algorithmes adaptés au problème (état de l’art).
* Prototypage de la chaine fonctionnelle complète, de l’acquisition d’image jusqu’à l’identification des objets. L’implémentation se fera sur la base de bibliothèques logicielles existantes (par ex. OpenCV).
* Implémentation séquentielle des algorithmes. L’objectif est de disposer du code source nécessaire et suffisant à la réalisation de la fonction, en réduisant au strict minimum les dépendances externes. Ce travail nécessite une bonne compréhension des algorithmes mis en œuvre.
* Parallélisation des algorithmes au moyen d’annotations OpenMP. 
* Intégration de la fonction sur la plateforme cible (RaspberryPi modèle 3)
* Analyse des possibilités d’accélération matérielle (FPGA), dans une première phase en utilisant OpenCL et, éventuellement, ou utilisant des accélérateurs matériels codés manuellement.
* Eventuellement : exploration architecturale (combinaison multi cœur / coprocesseur FPGA). [cette activité nécessitera des phases de travail à l’IRT Saint Exupery]
* Intégration de la fonction sur le robot (cette activité nécessitera des phases de travail à l’IRT Saint Exupery)

La phase de prototypage se fera en Python et/ou Scilab. La phase finale de développement se fera en C/C++.

